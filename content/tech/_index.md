---
date: 2022-10-14
lastmod: 2023-12-10
title: Technology
aliases:
  - tools
---

One of the keys to adaptable technology is composition -
taking components and combining them in different ways to form larger components.
Here are some of the components we are working on.
