---
date: 2023-04-01T05:00:00
lastmod: 2023-04-01
title: Versioned Nodes
authors:
 - Tristan
aliases:
 - /tools/converge
 - /tech/converge
---

[Versioned nodes][vn] are a data model for building collaborative local applications.

It generalizes and merges the ideas underlying [Tahoe-LAFS][tahoe], [git], [freenet], and other projects,
forming a new unit of data interchange better suited than files to
providing [provider-independent security][tahoe-pis] for
distributed collaboration with concurrent modification.

[tahoe-pis]: https://tahoe-lafs.readthedocs.io/en/latest/about-tahoe.html#what-is-provider-independent-security
[tahoe]: https://tahoe-lafs.org
[freenet]: https://freenetproject.org
[git]: https://git-scm.com
[vn]: https://vn.adpt.dev

<!--more-->

# Overview

Versioned nodes are a [data model][data-model] for applications to encode data
such that various [protocols][protocols] can synchronize nodes between applications.
The applications are connected together to form a network,
with data flowing through that network to each application that needs it.

[data-model]: https://vn.adpt.dev/core/node.html
[protocols]: https://vn.adpt.dev/protocol.html

```goat
   .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  . 

.                         .---------.          .---------.                       .
                         | user apps |        | user apps |                       
.                         '---------'          '---------'                       .
                               ^                    ^                            
.                              |                    |                            .
                               v                    v                            
.                         .---------.           .-------.                        .
      .----------.        | alice's |           | bob's |        .----------.    
.    | filesystem |<----->|  cache  |<--------->| cache |<----->| filesystem |   .
      '----------'        '---------'           '-------'        '----------'    
.                                 ^               ^                              .
                                  |   .-------.   |                              
.                                  '->| cloud |<-'    .----------.               .
                                      | store +----->| web server |              
.                                     '-------'       '----------'               .

   .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  . 
```

Data flows differently in node exchange networks than traditional ones.
In traditional networks, each host can send a message to any other host, by addressing it to that host.
Here, applications communicate by publishing a new version of a node,
and the change propagates through the network to other applications that subscribe to that node.

# Priorities

- Security:
  - Like [Tahoe-LAFS][tahoe], data should be encrypted such that when it is stored on a node,
    it's not readable without authorization, even by the node's operator.
  - Unlike Tahoe-LAFS, the presence or absence of even encrypted data must not be revealed across the network without authorization.

- Liveness:
  - Changes should be propagated quickly through the network.
  - Without further updates, the network will [eventually become consistent][eventual-consistency].

[eventual-consistency]: https://en.wikipedia.org/wiki/Eventual_consistency

- Safety:
  - Storage/cache nodes should allow data to be marked to be retained even it's not frequently requested.
  - Concurrent updates will mean that application nodes will receive the same updates in different orders.
    The network must provide enough information for the application to resolve those updates to identical states.

- Adaptability:
  - The operator should be able to choose different reliability and security profiles for different sets of data.
  - The network must support a wide variety of topologies and connectivity patterns.
