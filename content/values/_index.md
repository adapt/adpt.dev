---
date: 2022-10-14
lastmod: 2023-12-08
title: Values
---

We strive to promote individual and collective autonomy.

This is reflected in the essence of adaptable technology - its adaptability -
as well as in the technology that we choose to create.
The percolation of any technology through a society will have consequences for
the autonomy of its inhabitants.
So we must carefully consider the effects of any technology we devise,
for while many technologies enhance autonomy, some do not,
and some enhance some individuals' autonomy at the expense of others.

But what values go into making a particular piece of technology adaptable?
