---
date: 2022-11-12
lastmod: 2023-12-08
title: Tools are for Operators
authors:
 - Tristan
---

Tools should:

1. expose their natural functionality to the operator.
2. prioritize common or important uses.
3. protect the operator from accidental and incidental harm.

<!--more-->

## Natural Functionality

Radio transceivers are almost always restricted to the frequency ranges that are legal for the intended audience to transmit on and commonly used at the time of manufacture in the jurisdiction of the target market.
Inside lives a much more flexible piece of equipment - capable of receiving and transmitting multiple (often arbitrary) modes over a much wider frequency range (usually the entire band that is in use world-wide).

The frequency restrictions are mostly done to satisfy various regulatory bodies,
and it is legally difficult to get around that selling a commercial product.
The mode restrictions are partially in place because the level of access needed to reprogram the signal processing path is generally more than what's needed to bypass frequency restrictions.
But there's also an incentive to put a premium on multi-mode radios, even when the underlying hardware may not be that different.

## Common and Important Uses

Transceivers (and just plain receivers) are often programmed with specific commonly used channels and scan groups.
While the setup can be tedious, because of the vast flexibility of radio communications, it makes the normal use of the transceiver much easier.
"I'll be on channel 2." beats "I'll be on 144.340 MHz with an 88.2 Hz tone." any day.

This is an area where most transceivers are fairly successful.
Not that transceivers feature the pinnacle of user interface design,
just that they generally provide a sufficient interface for the most common tasks.

## Accidental and Incidental Harm

There is a sense in which the frequency restrictions protect operators from accidental harm,
as there are serious fines for misuse of spectrum.
At the same time, in a life-threatening situation any transmission necessary to successfully communicate the emergency is usually allowed, and this practice *prevents* that. 

Because a radio transmitter does not know when there's an emergency. Only the operator does.

So transceivers should be able to be programmed with channels that are outside their licensed frequency ranges and have an emergency override to allow transmitting on them.
