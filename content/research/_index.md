---
date: 2022-11-13
lastmod: 2022-11-13
title: Speculation and Research
---

The theoretical underpinnings of projects,
investigations into current industry standards,
and attempts to extract understanding from the surrounding chaos.
