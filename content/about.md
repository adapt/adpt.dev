---
date: 2022-11-13T18:38:36-05:00
lastmod: 2023-12-08
title: About
authors:
  - Tristan
---

Adaptable Research is dedicated to the creation and study of adaptable technology -
tools made for their operators to modify, build upon, and compose to suit their individual needs.

<!--more-->

To that end, these tools must be as [modular, extendable, simple, and universal](/values/) as possible.
They also need to be well documented, both in their theoretical workings and their practical usage.
We seek to craft excellent tools with equally excellent documentation.

We do not limit our view of technology to the modern high technology -
software, computers, electronics -
though there are certainly great gains to be made there.
Technology includes every object that humans have fashioned to accomplish some purpose.
Technology includes social structures,
group decision-making strategies,
and even the language we use to participate in them.
We seek to explore, document, and make adaptable all areas of technology.

Legal encumbrances and intellectual monopolies are artificial barriers to adapting technology.
We release all software, documentation, designs, diagrams, and other copyrightable works into the public domain
with dedications such as the [Unlicense](https://unlicense.org/) and [Creative Commons Zero](https://creativecommons.org/publicdomain/zero/1.0/).

## People

Adaptable Research is currently a one-person project of [trystimuli](https://try.st.imu.li).

I welcome any friendly and thoughtful collaborators who are ready to move slow and build things.
At the moment the designs, software, and documentation (including this website) live [on GitLab](https://gitlab.com/adapt).
That will probably migrate to something based on [versioned nodes](/tools/vn/) once the tools exist.
